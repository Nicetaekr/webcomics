﻿# Pepper&Carrot webcomics

This repository contains all the files necessary to translate the webcomics [Pepper&Carrot](http://www.peppercarrot.com). 

## Documentation

A simple how-to translate can be read on the file [CONTRIBUTING.md](https://framagit.org/peppercarrot/webcomics/blob/master/CONTRIBUTING.md), but in a nutshell you just need to install the fonts ( available in the subfolder fonts/ ),  get the repository and translate the SVG inside the episode folder with Inkscape.

## References

To keep consistency for the names of characters and place accross all the webcomics episodes, you can edit the LibreOffice spreadsheet file [translation-names-references.fods](https://framagit.org/peppercarrot/webcomics/blob/master/translation-names-references.fods) at the root of the repository.

## Credits

Read [AUTHORS.md](AUTHORS.md) in the root of this repository's tree for the full attribution, this file is public on the main website: ["Author" category](https://www.peppercarrot.com/static7/author).

## Sources artworks

This repository contains only low resolution artworks for editing translation. Do not edit or propose a commit for the artwork in these repository. For rendering the page at 300ppi, use the high resolution graphics available [on the main server](http://www.peppercarrot.com/0_sources/).

## Fonts

The open fonts necessary to translate Pepper&Carrot are in the subfolder fonts.
For documentation and their license, read [fonts/README.md](fonts/README.md).

# License

Content in this repository is licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License, except for the files in the `fonts/` directory, which are released under their own separate license agreements.

By submitting content to this repository, one agrees to the [contributor's terms](CONTRIBUTING.md).
